jQuery(document).ready(function ($) {
    // AJAX function to get book data in JSON format
    function getBooksData() {
        $.ajax({
            url: 'http://localhost/fooztest/wp-admin/admin-ajax.php',
            type: 'GET',
            dataType: 'json',
            data: {
                action: 'get_books_json',
            },
            success: function (response) {
                // Process the JSON data
                console.log(response); // You can use this data to display books on the frontend.
            },
            error: function (errorThrown) {
                console.log('Error: ' + errorThrown);
            }
        });
    }

    // Call the AJAX function when needed
    getBooksData();
});