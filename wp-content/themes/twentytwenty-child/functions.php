<?php

// Enqueue child theme's CSS
function twentytwenty_child_enqueue_styles() {
    // Load the parent theme stylesheet
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

    // Load the child theme stylesheet
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('parent-style') );
}
add_action( 'wp_enqueue_scripts', 'twentytwenty_child_enqueue_styles' );

// Enqueue custom JavaScript file in the footer
function enqueue_custom_scripts() {
    // Register and enqueue custom script
    wp_enqueue_script(
        'custom-scripts',
        get_stylesheet_directory_uri() . '/assets/js/scripts.js', 
        array('jquery'), // Dependencies - jQuery in this case
        '1.0',
        true // Load in the footer (set to true)
    );
}
add_action('wp_enqueue_scripts', 'enqueue_custom_scripts');


//Register custom post types

function custom_books_post_type() {
    $labels = array(
        'name'                  => __('Books', 'Post Type General Name', 'fooztest'),
        'singular_name'         => __('Book', 'Post Type Singular Name', 'fooztest'),
        'menu_name'             => __('Books', 'fooztest'),
        'name_admin_bar'        => __('Book', 'fooztest'),
        'archives'              => __('Book Archives', 'fooztest'),
        'attributes'            => __('Book Attributes', 'fooztest'),
        'parent_item_colon'     => __('Parent Book:', 'fooztest'),
        'all_items'             => __('All Books', 'fooztest'),
        'add_new_item'          => __('Add New Book', 'fooztest'),
        'add_new'               => __('Add New', 'fooztest'),
        'new_item'              => __('New Book', 'fooztest'),
        'edit_item'             => __('Edit Book', 'fooztest'),
        'update_item'           => __('Update Book', 'fooztest'),
        'view_item'             => __('View Book', 'fooztest'),
        'view_items'            => __('View Books', 'fooztest'),
        'search_items'          => __('Search Book', 'fooztest'),
        'not_found'             => __('Not found', 'fooztest'),
        'not_found_in_trash'    => __('Not found in Trash', 'fooztest'),
        'featured_image'        => __('Featured Image', 'fooztest'),
        'set_featured_image'    => __('Set featured image', 'fooztest'),
        'remove_featured_image' => __('Remove featured image', 'fooztest'),
        'use_featured_image'    => __('Use as featured image', 'fooztest'),
        'insert_into_item'      => __('Insert into Book', 'fooztest'),
        'uploaded_to_this_item' => __('Uploaded to this Book', 'fooztest'),
        'items_list'            => __('Books list', 'fooztest'),
        'items_list_navigation' => __('Books list navigation', 'fooztest'),
        'filter_items_list'     => __('Filter books list', 'fooztest'),
    );

    $args = array(
        'label'                 => __('Book', 'fooztest'),
        'description'           => __('Books custom post type', 'fooztest'),
        'labels'                => $labels,
        'supports'              => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-book',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'rewrite'               => array('slug' => 'library'),
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );

    register_post_type('books', $args);
}
add_action('init', 'custom_books_post_type', 0);

// Register Custom Taxonomy
function custom_book_genre_taxonomy() {
    $labels = array(
        'name'                       => __('Genres', 'Taxonomy General Name', 'fooztest'),
        'singular_name'              => __('Genre', 'Taxonomy Singular Name', 'fooztest'),
        'menu_name'                  => __('Genre', 'fooztest'),
        'all_items'                  => __('All Genres', 'fooztest'),
        'parent_item'                => __('Parent Genre', 'fooztest'),
        'parent_item_colon'          => __('Parent Genre:', 'fooztest'),
        'new_item_name'              => __('New Genre Name', 'fooztest'),
        'add_new_item'               => __('Add New Genre', 'fooztest'),
        'edit_item'                  => __('Edit Genre', 'fooztest'),
        'update_item'                => __('Update Genre', 'fooztest'),
        'view_item'                  => __('View Genre', 'fooztest'),
        'separate_items_with_commas' => __('Separate genres with commas', 'fooztest'),
        'add_or_remove_items'        => __('Add or remove genres', 'fooztest'),
        'choose_from_most_used'      => __('Choose from the most used', 'fooztest'),
        'popular_items'              => __('Popular Genres', 'fooztest'),
        'search_items'               => __('Search Genres', 'fooztest'),
        'not_found'                  => __('Not Found', 'fooztest'),
        'no_terms'                   => __('No genres', 'fooztest'),
        'items_list'                 => __('Genres list', 'fooztest'),
        'items_list_navigation'      => __('Genres list navigation', 'fooztest'),
    );

    $args = array(
        'labels'             => $labels,
        'hierarchical'       => true,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_admin_column'  => true,
        'show_in_nav_menus'  => true,
        'show_tagcloud'      => true,
        'rewrite'            => array('slug' => 'book-genre'),
    );

    register_taxonomy('book_genre', array('books'), $args);
}
add_action('init', 'custom_book_genre_taxonomy', 0);

// Add image size
add_image_size( 'book_thumb', 250, 355 );
add_image_size( 'book_cover', 400, 568 );


//Shordcodes

//Most recent book title
function get_most_recent_book_title() {
    $args = array(
        'post_type'      => 'books',
        'posts_per_page' => 1,
        'orderby'        => 'date',
        'order'          => 'DESC',
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            $title = '<h3><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>';
            return $title;
        }
        wp_reset_postdata();
    }

    return 'No recent books found.';
}
add_shortcode('most_recent_book_title', 'get_most_recent_book_title');

//List of 5 Books from Given Genre:

function get_books_by_genre($atts) {
    $atts = shortcode_atts(array(
        'genre_id' => '',
    ), $atts);

    $args = array(
        'post_type'      => 'books',
        'posts_per_page' => 5,
        'orderby'        => 'title',
        'order'          => 'ASC',
        'tax_query'      => array(
            array(
                'taxonomy' => 'book_genre',
                'field'    => 'term_id',
                'terms'    => $atts['genre_id'],
            ),
        ),
    );

    $query = new WP_Query($args);

    $output = '';

    if ($query->have_posts()) {
        $output .= '<ul>';

        while ($query->have_posts()) {
            $query->the_post();
            $output .= '<li>';
            $output .= '<a href="' . get_permalink() . '">';
            $output .= get_the_title() . '</a>';
            $output .= '</li>';
        }

        $output .= '</ul>';

        wp_reset_postdata();
    } else {
        $output .= 'No books found in this genre.';
    }

    return $output;
}
add_shortcode('books_by_genre', 'get_books_by_genre');

// Callback function to retrieve book data and return it in JSON format
function get_books_json() {
    $args = array(
        'post_type'      => 'books',
        'posts_per_page' => 20,
    );

    $query = new WP_Query($args);
    $books = array();

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            $book = array(
                'name'   => get_the_title(),
                'date'   => get_the_date(),
                'genre'  => wp_get_post_terms(get_the_ID(), 'book_genre', array('fields' => 'names')),
                'excerpt' => get_the_excerpt(),
            );
            $books[] = $book;
        }
        wp_reset_postdata();
    }

    wp_send_json($books);
}

// Register the AJAX callback for logged-in users
add_action('wp_ajax_get_books_json', 'get_books_json');

// Register the AJAX callback for non-logged-in users
add_action('wp_ajax_nopriv_get_books_json', 'get_books_json');





