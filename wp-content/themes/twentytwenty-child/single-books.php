<?php
// This is the custom single book template

get_header();

?>

<section class="book-single">
    <div class="section-inner">
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            $book_genre = get_the_terms(get_the_ID(), 'book_genre');
            $book_genre_name = $book_genre ? $book_genre[0]->name : '';
            $book_date = get_the_date();

            ?>
            <article <?php post_class(); ?>>
                <header class="entry-header book-header">
                    <div class ="entry-header-inner section-inner medium book-header-inner">
                        <?php if (has_post_thumbnail()) : ?>
                            <figure class="book-image">
                                <?php the_post_thumbnail('book_cover'); ?>
                        </figure>
                        <?php endif; ?>
                        <div class="book-header-inner-heading">
                            <p class="book-genre"><?php echo $book_genre_name; ?></p>
                            <h1 class="book-title"><?php the_title(); ?></h1>
                            <p class="book-date"><?php echo $book_date; ?></p>
                        </div> 
                    </div>
                </header>
            </article>
            <?php
        }
    }

    ?>
    </div>
</section>


<?php
get_footer();

