<?php
// This is the custom archive template for Books post type

get_header();
?>

<section class="books-archive">
    <header class="entry-header has-text-align-center">
        <div class ="entry-header-inner section-inner">
            <h1>Library</h1>
        </div>
    </header>

    <div class="section-inner">
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                $book_date = get_the_date();
                $book_genres = get_the_terms(get_the_ID(), 'book_genre');
                ?>

                <article <?php post_class(); ?>>  
                    <div class="book-card">
                        <?php if (has_post_thumbnail()) : ?>
                            <figure class="book-image">
                                <?php the_post_thumbnail('book_thumb'); ?>
                            </figure>
                        <?php endif; ?>
                        <div class="book-card-content">
                            <?php if ($book_genres && !is_wp_error($book_genres)) : ?>
                                <p>
                                    <?php
                                    $genre_links = array();
                                    foreach ($book_genres as $genre) {
                                        $genre_links[] = '<a href="' . esc_url(get_term_link($genre)) . '">' . esc_html($genre->name) . '</a>';
                                    }
                                    echo implode(', ', $genre_links);
                                    ?>
                                </p>
                            <?php endif; ?>
                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <p><?php echo $book_date; ?></p>
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                </article>

                <?php
            }

            // Pagination
            if (function_exists('paginate_links')) {
                echo '<div class="pagination">';
                echo paginate_links(array(
                    'prev_text' => '&laquo; Previous',
                    'next_text' => 'Next &raquo;',
                ));
                echo '</div>';
            }

            wp_reset_postdata();
        } else {
            echo '<p>No books found.</p>';
        }
        ?>
    </div>
</section>

<?php
get_footer();
?>
