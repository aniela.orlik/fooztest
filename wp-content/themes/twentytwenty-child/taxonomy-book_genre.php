<?php
// This is the custom genre page (taxonomy) template

get_header();
?>
<section class="books-archive">
    <?php
    $term = get_queried_object();
    $taxonomy_label = get_taxonomy($term->taxonomy)->labels->singular_name;
    $archive_title = single_term_title('', false);
    ?>

    <header class="entry-header has-text-align-center">
        <div class ="entry-header-inner section-inner">
            <h1><?php echo $taxonomy_label; ?> - <?php echo $archive_title; ?> </h1>
        </div>
    </header>

    <div class="section-inner">

    <?php
    $books_per_page = 5;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => 'books',
        'posts_per_page' => $books_per_page,
        'paged' => $paged,
    );

    $query = new WP_Query($args);

    //jestem tutaj

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            ?>
            <article <?php post_class(); ?>>
                <div class="book-card">
                    <?php if (has_post_thumbnail()) : ?>
                        <div class="book-image">
                            <?php the_post_thumbnail('book_thumb'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="book-card-content">
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <p><?php echo get_the_date(); ?></p>
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            </article>
            <?php
        }

        // Pagination
        if (function_exists('paginate_links')) {
            echo '<div class="pagination">';
            echo paginate_links(array(
                'prev_text' => '&laquo; Previous',
                'next_text' => 'Next &raquo;',
            ));
            echo '</div>';
        }

        wp_reset_postdata();
    }
    ?>
</section>
<?php
get_footer();

